use specs;
use constants::Resource;

pub struct Cost {
    pub resources: Vec<Resource>,
}

impl specs::Component for Cost {
    type Storage = specs::VecStorage<Cost>;
}
// impl Cost {
//     pub fn new(res: Vec<Resource>) -> Cost {
//         Cost {
//             resources: res,
//         }
//     }
// }
