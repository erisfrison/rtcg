use specs;
use symtern;
use constants;

pub struct Card {
    pub id: usize,
    pub set_id: usize,
    pub title: String,
    pub owner: Option<specs::Entity>, //Player entity
    pub card_type: String,
    pub subtypes: Vec<symtern::Sym<u16>>,
    pub effects: Vec<constants::effect::Effect>,
}

impl specs::Component for Card {
    type Storage = specs::VecStorage<Card>;
}
