use cards::card::Card;
use specs;

///This component should be used for sets of cards
///for example when the player draws the top 3 cards
///and chooses one. It should probably be attached to
///the effect entity. But I have to think that through.
pub struct Cards {
    cards: Vec<specs::Entity>,
}

impl specs::Component for Cards {
    type Storage = specs::VecStorage<Cards>;
}
