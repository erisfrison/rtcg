use specs;

pub struct Monster {
    pub health: i8,
    pub attack: i8,
}
impl specs::Component for Monster {
    type Storage = specs::VecStorage<Monster>;
}
impl Monster {
    pub fn new(health: i8, attack: i8) -> Monster {
        Monster {
            health: health,
            attack: attack,
        }
    }
}
