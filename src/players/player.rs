use specs;
use constants::Resource;
use rand;
use rand::Rng;

pub struct Player {
    pub name: String,
    pub human: bool,
    pub max_life: i32,
    pub life: i32,
    pub hand: Vec<specs::Entity>,
    pub deck: Vec<specs::Entity>,
    pub hell: Vec<specs::Entity>,
    pub play: Vec<specs::Entity>,
    pub out_of_cards: bool,
    //True if the player has already played an essence.
    //only Essence cards played from the hand count.
    pub played_essence: bool,
}

impl specs::Component for Player {
    type Storage = specs::VecStorage<Player>;
}

impl Player {
    pub fn shuffle_deck(&mut self) {
        rand::thread_rng().shuffle(&mut self.deck);
        // let mut rng = rand::thread_rng();
        // println!("{}",rng.gen::<i32>());
        // //TODO: Shuffle deck.
    }
}

/**
  * Component containing the resources available to player.
  * - Air, Earth, Fire, Water
  * - Ice, Lightning, Magma, Poison
  * - Day, Night, Pure, Steam
  * Other resources in the future could be things like:
  * - Soul Tokens
  * - Energy Tokens
  * - etc.
  */
pub struct ResourcePool {
    pub resources: Vec<Resource>,
    pub in_use: Vec<Resource>,
}

impl specs::Component for ResourcePool {
    type Storage = specs::VecStorage<ResourcePool>;
}

impl ResourcePool {
    pub fn new(res: Vec<Resource>) -> ResourcePool {
        ResourcePool {
            resources: res.clone(),
            in_use: res,
        }
    }
}
