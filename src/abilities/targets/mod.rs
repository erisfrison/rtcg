//! The components like SingleTarget, TargetAll, MultiTarget, TargetOwnedByPlayer, TargetInHand
//! can be used together, this should probably not be done, but it can be done.

use specs;
use specs::{Entity,RunArg,Component,World,Join,Gate};
use cards::card::Card;

///This function return the entities of all modifiers that affect card e.
///or at least it is supposed to, currently it returns an empty vec
pub fn targets_card(e: Entity, arg: &RunArg) -> Vec<Entity> {
    let cards = arg.fetch(|w| w.read::<Card>());
    //We will need the owner of this card;
    //Maybe extend to (owner, cardtype) depending on implementation
    let owner: Option<Entity> = {
        if let Some(c) = cards.get(e) {
            c.owner
        } else { None }
    };

    println!("{:?}",owner);

    //
    // TODO: Search through all Components with a variation of target and check if Entity e
    // is a target of the effect in question.
    //

    return Vec::new(); //TODO: Temporary empty return value;
}

pub fn targets_player(ep: Entity, world: &World) -> Vec<Entity> {
    let mut entities = Vec::new();
    {
        let ets = world.read::<PlayerTarget>().pass();
        for (et,eid) in (&ets, &world.entities()).join() {
            if et.target == ep {
                entities.push(eid);
            }
        }
    }
    return entities;
}
///Targets a single player
pub struct PlayerTarget {
    pub target: Entity,
}

impl specs::Component for PlayerTarget {
    type Storage = specs::VecStorage<PlayerTarget>;
}
//TODO: add TargetAllPlayers, TargetAllOtherPlayers, TargetTeam, TargetOtherTeams

///Targets a single card
pub struct SingleTarget {
    target: Entity,
}

impl specs::Component for SingleTarget {
    type Storage = specs::VecStorage<SingleTarget>;
}

// trait Targets {
//     fn is_target(&self, e: Entity, w: &RunArg) -> bool;
// }
//
// /**
//   *  SingleTarget
//   *  Target a single card.
//   */
//
// pub struct SingleTarget {
//     target: Entity, //Entity = the target
// }
//
// impl Targets for SingleTarget {
//     fn is_target(&self, e: Entity, w: &RunArg) -> bool {
//         if e == self.target {
//             return true;
//         }
//         return false;
//     }
// }
//
// /**
//   *  PlayerOwnedTarget
//   *  Target all the cards owned by player.
//   */
// pub struct PlayerOwnedTarget {
//     target_owner: Entity, //All cards owned by Entity
// }
//
// impl Targets for PlayerOwnedTarget {
//     fn is_target(&self, e: Entity, arg: &RunArg) -> bool {
//         let cards = arg.fetch(|w| w.read::<Card>());
//         if let Some(c) = cards.get(e) {
//             return c.owner == self.target_owner;
//         }
//         return false;
//     }
// }
//
// /**
//   *  MultiTarget
//   *  Target a number of cards.
//   */
// pub struct MultiTarget {
//     targets: Vec<Entity>, //TODO: Would it be better to use a set?
// }
//
// impl Targets for MultiTarget {
//     fn is_target(&self, e: Entity, arg: &RunArg) -> bool {
//         return self.targets.contains(&e);
//     }
// }
