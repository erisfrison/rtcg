pub mod cardlocation;
pub mod tappedstatus;
pub mod addtoresourcepool;
use constants::effect;
use specs;

pub struct Effects {
    pub effects: Vec<effect::Effect>,
}
impl specs::Component for Effects {
    type Storage = specs::VecStorage<Effects>;
}
