use specs;
use constants::Resource;

pub struct AddToResourcePool {
    pub resource: Resource,
}
impl specs::Component for AddToResourcePool {
    type Storage = specs::VecStorage<AddToResourcePool>;
}
