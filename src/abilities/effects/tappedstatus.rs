use specs;

pub struct TappedStatus {
    status: bool,
}
impl specs::Component for TappedStatus {
    type Storage = specs::VecStorage<TappedStatus>;
}
