//!It is important that +x/+x counters are not linked to the source from which they came.
//!They are not an effect.
use specs;

///For effects that are removed after the last step of the current turn;
pub struct UntilEndOfTurn;
impl specs::Component for UntilEndOfTurn {
    type Storage = specs::VecStorage<UntilEndOfTurn>;
}

///For effects that last until the stack of which they are part has been resolved;
pub struct UntilStackResolve;
impl specs::Component for UntilStackResolve {
    type Storage = specs::VecStorage<UntilStackResolve>;
}

///Another way of saying it doesn't stop;
//TODO: think about whether we really need this?
// pub struct UntilEndOfGame;
// impl specs::Component for UntilEndOfGame {
//     type Storage = specs::VecStorage<UntilEndOfGame>;
// }

///For effects that last until the end of the nearest combat phase.
///Generally these will also be activated during the combat phase,
///but some there will probably be exceptions.
pub struct UntilEndOfCombat;
impl specs::Component for UntilEndOfCombat {
    type Storage = specs::VecStorage<UntilEndOfCombat>;
}

///For effects that are removed after the last step of the turn of the player before you.
///That player is identified through the id.
pub struct UntilYourNextTurn {
    id: specs::Entity,
}
impl specs::Component for UntilYourNextTurn {
    type Storage = specs::VecStorage<UntilYourNextTurn>;
}
