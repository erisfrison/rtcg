use specs;

pub struct Ability {
    pub name: String,
}

impl specs::Component for Ability {
    type Storage = specs::VecStorage<Ability>;
}
