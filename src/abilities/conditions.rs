use specs;
use constants::Condition;

pub struct Conditions {
    pub conditions: Condition,
    pub satisfied: bool,
}

impl specs::Component for Conditions {
    type Storage = specs::VecStorage<Conditions>;
}
