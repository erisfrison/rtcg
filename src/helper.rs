use specs::{World,Entity,Gate};
use constants::elements::*;
use constants::Resource;
use constants::effect;
use cards::card::Card;
use cards::monster::Monster;
use cards::cards::Cards;
use cards::cost::Cost;
use systems;
use std::io;
use std::io::Write;
use players::player::{Player,ResourcePool};
use abilities::{effects,targets,duration};
use abilities::conditions::Conditions;
use std::collections::HashMap;
use carddb::card::CardData;

/**Currently the game is played through the command line
  *The functions for this feature will for now be placed
  *here, but they should probably be moved to a location
  *that is dedicated to them.
  */

pub fn create_world() -> World {
        let mut w = World::new();
        w.register::<Player>();
        w.register::<ResourcePool>();
        w.register::<effects::addtoresourcepool::AddToResourcePool>();
        w.register::<duration::UntilEndOfTurn>();
        w.register::<targets::PlayerTarget>();
        w.register::<Card>();
        w.register::<Cost>();
        w.register::<Monster>();
        w.register::<Cards>();
        w.register::<Conditions>();
        return w;
}

pub fn create_player(name: String) -> Player {
    Player {
        name: name,
        human: true,
        max_life: 100,
        life: 100,
        hand: Vec::new(),
        deck: Vec::new(),
        hell: Vec::new(),
        play: Vec::new(),
        out_of_cards: false,
        played_essence: false,
    }
}

pub fn create_card(data: CardData, w: &mut World) -> Entity {
    let mut entity = w.create_now()
        .with( Card {
                id: data.card_id,
                set_id: data.set_id,
                title: data.name,
                card_type: data.card_type,
                subtypes: data.subtypes,
                effects: data.effects,
                owner: None,
        });

    if !data.cost.is_empty() {
        entity = entity.with( Cost {
            resources: data.cost,
        });
    }
    if !data.health.is_none() && !data.attack.is_none() {
        entity = entity.with( Monster {
            health: data.health.unwrap(),
            attack: data.attack.unwrap(),
        });
    }
    entity = entity.with( Conditions {
        conditions: data.condition,
        satisfied: true,
    });
    return entity.build()
}

// pub fn create_essence(e: Element, w: &mut World) -> Entity {
//     w.create_now()
//         .with( Card {
//             id: 0,
//             set_id: 0,
//             title: "Essence of ".to_string()+e.get_element_string().as_str(),
//             owner: None,
//         })
//         .with( Essence {
//             essence: e,
//         })
//         .build()
// }
//
// pub fn create_monster(h: i32, a: i32, w: &mut World) -> Entity {
//     w.create_now()
//         .with( Card {
//             id: 0,
//             set_id: 0,
//             title: "Test Monster".to_string(),
//             owner: None,
//         })
//         .with( Monster {
//             health: h,
//             attack: a,
//         })
//         .with( Cost {
//             resources: vec![Resource::Essence(Element::Classical(ClassicalElement::Earth))],
//         })
//         .build()
// }

pub fn process_player_input(ep: Entity, w: &mut World) -> bool {
    loop {
        let mut input = String::new();
        print!(">");
        io::stdout().flush();
        if let Ok(n) = io::stdin().read_line(&mut input) {
            let v: Vec<&str> = input.split(' ').collect();
            match v[0].trim() {
                "exit" => { return true; },
                "play" => {
                    if v.len()>1 { //check if argument is supplied
                        if let Ok(ci) = v[1].trim().parse::<usize>() { //argument is valid usize
                            let cards = systems::playersystem::get_player_hand(ep,w);
                            if ci<cards.len() { //argument is < #no cards
                                systems::playersystem::play_card(ep,cards[ci],w);
                            }
                        }
                    }
                },
                "pay" => {
                    systems::playersystem::pay_resources(ep,vec![Resource::Essence(Element::Classical(ClassicalElement::Earth))],w);
                    print_available_resources(ep,w);
                },
                "pass" => { break; },
                _ => { },
            }
        }
    }
    return false;
}

use colored::*;
pub fn print_cards(cards: Vec<Entity>, w: &World) {
    let cards_w = w.read::<Card>().pass();
    let conds_w = w.read::<Conditions>().pass();
    for i in 0..cards.len() {
        if let (Some(c),Some(cond)) = (cards_w.get(cards[i]),conds_w.get(cards[i])) {
            if cond.satisfied {
                print!("[{}] {}",i,c.title.green());
            }
            else {
                print!("[{}] {}",i,c.title);
            }
            let monsters = w.read::<Monster>().pass();
            if let Some(m) = monsters.get(cards[i]) {
                print!(" ({},{})",m.health,m.attack);
            }
            println!("");
        }
        else {
            println!("Invalid card!");
        }
    }
}

pub fn print_resources(res: &Vec<Resource>) {
    let mut resmap = HashMap::new();
    for r in res {
        let counter = resmap.entry(r).or_insert(0);
        *counter += 1;
    }
    let mut i = 0;
    for (r,j) in &resmap {
        print!("{} ({})",r.get_resource_string(),j);
        if resmap.len()-1>i {
            print!(", ");
        }
        else { println!(""); }
        i+=1;
    }
}

pub fn print_available_resources(ep: Entity, w: &World) {
    let resources = systems::playersystem::get_player_available_resources(ep,w);
    if resources.len()>0 {
        print!("Available resources: ");
    } else { println!("No resources available."); return; }
    print_resources(&resources);
    println!("");
}
