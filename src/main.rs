#![warn(missing_docs)]

//!
// ____ _____ ____ ____
//|  _ \_   _/ ___/ ___|
//| |_) || || |  | |  _
//|  _ < | || |__| |_| |
//|_| \_\|_| \____\____|
//
//!
//!#Rust: The Card Game
//!
//!This is the documentation for rtcg

mod cards;
mod constants;
mod abilities;
mod players;
mod carddb;
extern crate specs;
extern crate xml;
extern crate rand;
extern crate symtern;
extern crate colored;
// use specs::Join;
//#[macro_use] extern crate macro_attr;

pub use constants::elements::{Element,ClassicalElement,CombinedElement,SpecialElement};
pub use constants::{TurnPhase,Resource};
use players::player::{ResourcePool};
pub use abilities::effects::*;
pub use abilities::targets;
mod helper;
mod systems;
use std::collections::VecDeque;
use std::sync::{RwLock,Arc};

fn main() {
    let mut w = helper::create_world();
    let mut subtype_pool = symtern::Pool::<str,u16>::new();
    carddb::filereader::import_subtypes("./db/subtypes",&mut subtype_pool);
    carddb::filereader::import_card_xml("./db/earth.xml",&mut subtype_pool);
    carddb::filereader::read_databases();

    let pid1 = w.create_now().with(helper::create_player("Me".to_string()))
        .with(ResourcePool::new(Vec::new())).build();
    let pid2 = w.create_now().with(helper::create_player("Default".to_string()))
        .with(ResourcePool::new(Vec::new())).build();

    //let p: &Player = &w.read_resource::<Player>().wait();
    //println!("{}", &p.get_name());;

    // planner.run_custom(move |arg| {
    //     let players = arg.fetch(|w| w.read::<Player>());
    //     let p = players.get(pid);
    //     println!("I got player {}", p.unwrap().get_name());
    // });

    // planner.run1w0r(|e: &mut Player| {
    //
    //     e.lose_life(5);
    //     if e.is_human() {
    //         e.gain_life(5);
    //     }
    // });

    //Create some cards and pass put them into a deck.
    let mut cido = Vec::new();
    while cido.len()<60 {
        cido.push(helper::create_card(carddb::filereader::import_card_xml("./db/earth.xml",&mut subtype_pool),&mut w));
        cido.push(helper::create_card(carddb::filereader::import_card_xml("./db/night.xml",&mut subtype_pool),&mut w));
        cido.push(helper::create_card(carddb::filereader::import_card_xml("./db/testmonster.xml",&mut subtype_pool),&mut w));
        cido.push(helper::create_card(carddb::filereader::import_card_xml("./db/dragon.xml",&mut subtype_pool),&mut w));
    }
    while let Some(cid) = cido.pop() {
        systems::playersystem::add_to_deck(pid1,cid,&w);
        if let Some(cid) = cido.pop() {
            systems::playersystem::add_to_deck(pid2,cid,&w);
        }
    }
    systems::playersystem::shuffle_deck(pid1,&w);

    let mut turn = 0;
    let mut turn_phase = Arc::new(RwLock::new(TurnPhase::Untap));
    let mut player_queue = VecDeque::new();
    player_queue.push_back(pid1);
    player_queue.push_back(pid2);


    for _ in 0..7 {
        systems::playersystem::draw_card(pid1,&w);
    }

    let mut planner = specs::Planner::<()>::new(w);
    planner.add_system(systems::conditionsystem::ConditionSystem {
        turn_phase: turn_phase.clone(),
    },"ConditionSystem",20);
    loop {
        let pid = *player_queue.front().unwrap();
        planner.dispatch(());
        planner.wait();
        let mut current_phase = turn_phase.write().unwrap();
        println!("### {} Phase ###",current_phase.to_string());
        let mut w = planner.mut_world();
        match *current_phase {
            TurnPhase::Untap => {

                // w.create_now().with(targets::PlayerTarget {target:pid}).with(addtoresourcepool::AddToResourcePool::new(vec![Resource::SoulToken])).build();

                //Untap resources for current player:
                systems::playersystem::untap_resources(pid,&w);
                helper::print_available_resources(pid,&w);
            }
            TurnPhase::Draw => {
                print!("Drawing card.");
                if !systems::playersystem::draw_card(pid,&w) { break; };
                println!(" (Remaining: {})",systems::playersystem::get_player_deck_size(pid,&w));
            }
            TurnPhase::Resource => {
                println!("Your hand:");
                let hand = systems::playersystem::get_player_hand(pid,&w);
                helper::print_cards(hand,&w);
                if helper::process_player_input(pid,&mut w) { break; }
            }
            TurnPhase::Build => {
                println!("Your hand:");
                let hand = systems::playersystem::get_player_hand(pid,&w);
                helper::print_cards(hand,&w);
                if helper::process_player_input(pid,&mut w) { break; }
            }
            TurnPhase::Combat => {
                if helper::process_player_input(pid,&mut w) { break; }
            }
            TurnPhase::Recovery => {
                if helper::process_player_input(pid,&mut w) { break; }
            }
            _ => { if turn>10 { break; } }
        }
        if *current_phase == TurnPhase::EndTurn {
            let current_player = player_queue.pop_front().unwrap();
            player_queue.push_back(current_player);
            // if turn>1 {
            //     systems::playersystem::discard_random_card(pid,&w);
            // }
            println!("\n~ End of turn ~");
            turn+=1;
            println!("
            ###############
                Turn {}
            ###############", turn/player_queue.len()+1);
        }
        let next_phase = current_phase.next_phase();
        *current_phase = next_phase;
    }
    //let mut planner = specs::Planner::<()>::new(w);

    // planner.run0w1r(|e: &Player| {
    //     println!("{}: {}, {}", e.name, e.life, e.out_of_cards);
    // });
    // planner.run0w1r(|e: &Essence| {
    //     println!("{}", e.essence.to_string());
    // });

    //let element : Element = Element::Classical(ClassicalElement::Air);
    println!("Game over!");
    planner.wait();
}

#[cfg(test)]
mod tests {
    use carddb;
    use symtern;
    use symtern::prelude::*;
    #[test]
    //Test the subtype importing from text file
    fn subtype_importing_subtypes_from_txt() {
        let mut subtype_pool = symtern::Pool::<str,u16>::new();
        carddb::filereader::import_subtypes("./db/subtypes",&mut subtype_pool);
        let beast = subtype_pool.intern("Beast").unwrap();
        assert!(subtype_pool.resolve(beast)==Ok("Beast"));
    }
    #[test]
    fn test_print_xml() {
        carddb::filereader::print_xml("./db/earth.xml");
    }
    // #[test]
    // fn subtype_importing_from_serialized() {
    //     panic!("Not implemented yet!");
    // }
    #[test]
    fn card_importing_from_xml() {
        let mut subtype_pool = symtern::Pool::<str,u16>::new();
        let card = carddb::filereader::import_card_xml("./db/earth.xml",&mut subtype_pool);
        assert!(card.name=="Essence of Earth".to_string());
    }
    // #[test]
    // fn card_importing_from_serialized() {
    //     panic!("Not implemented yet!");
    // }
}
