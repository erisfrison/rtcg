use constants::Resource;
use specs::{Entity,EntityBuilder,World};
use abilities::effects::*;

#[derive(Clone,Eq,PartialEq)]
pub enum Effect {
    Dummy,
    AddToResourcePool(Resource),
    PlayedResource(bool),
    DisableRandomEssence,
    CardLocation,
    TappedStatus,
}

impl Effect {
    pub fn attach_to_entity(&self, ec: Entity, w: &World) -> Entity {
        let mut entity = EntityBuilder::new(ec,w);
        match self {
            &Effect::AddToResourcePool(ref res) => {
                entity = entity.with(addtoresourcepool::AddToResourcePool {
                    resource: res.clone(),
                });
            }
            _ => {}
        }
        entity.build()
    }
}
