use specs::Entity;

pub enum TargettingType {
    SingleTarget,
    TargetAll,
    TargetOwnedByPlayer,
    TargetInHand,
    IsAnyTypes, //Is any of the types in vector
    IsAllTypes, //Is all of the types in vector
    MonsterHealthLessThan,
    MonsterHealthGreaterThan,
    MonsterAttackLessThan,
    MonsterAttackGreaterThan,
    MonsterSelf,

    //The following Targets target Players and not cards
    PlayerSelf,
    PlayerTarget(Entity),
    PlayerHealthLessThan,
    PlayerHealthGreaterThan,
}
