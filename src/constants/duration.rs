pub enum Duration {
    // Permanent,
    UntilEndOfTurn,
    UntilStackResolve,
    WhileInGraveyard,
    UntilEndOfCombat,
    UntilEndOfStep,
    UntilTargetNextTurn,
    UntilTargetLeavesPlay,
}
