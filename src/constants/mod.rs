#![allow(dead_code)]
pub mod elements;
pub mod duration;
pub mod effect;
pub mod target;
use Element;

///Resources are used to play cards and activate abilities.
#[derive(PartialEq,Eq,Clone,Hash)]
pub enum Resource {
    ///For Essence resources
    Essence(Element),
    ///A resource I might want to use in the future,
    ///really just a placeholder for now.
    SoulToken,
}

impl Resource {
    pub fn from_str(r: &str) -> Resource {
        match r {
            "Soul Token" => return Resource::SoulToken,
            _ => return Resource::Essence(Element::from_str(r)),
        }
    }
    ///Returns a human readable string of the resource.
    pub fn get_resource_string(&self) -> String {
        match *self {
            Resource::Essence(ref e) => e.get_element_string(),
            Resource::SoulToken => "Soul".to_string(),
        }
    }
}

pub enum PlayerDeathCause {
    OutOfCards,
    OutOfHealth,
}

pub enum Condition {
    And(Vec<Condition>),
    Or(Vec<Condition>),
    Phase(TurnPhase),
    True,
    // Xor(Vec<Condition>),
    // Nor(Vec<Condition>),
    // Not(Box<Condition>),
    // Always,
    ResourcePhase,
    HasNotPlayedResource,
}

///All the possible phases of a turn.
#[derive(PartialEq)]
pub enum TurnPhase {
    ///During this phase the players tapped cards are untapped
    ///The invested resources are returned.
    Untap,
    ///The player whose turn it is draws a card
    ///If they are unable to draw a card because their deck is empty
    ///then they lose.
    Draw,
    ///During the resource phase only essence cards can be played (usually)
    Resource,
    ///The build phase is where most cards come into play
    Build,
    ///The combat phase is where creatures fight it out with each other
    Combat,
    ///The recovery phase is like a second build phase for creatures,
    ///but also the time for recovery cards.
    ///Recovery cards generally require certain goals to have been
    ///met during the combat phase to achieve their potential.
    ///The player can also let creatures rest until the nex turn
    ///so that they regenerate health.
    Recovery,
    ///Things are cleaned up
    EndTurn,
}
impl TurnPhase {
    ///This function cycles through the phases of a turn.
    pub fn next_phase(&self) -> TurnPhase {
        match *self {
            TurnPhase::Untap => TurnPhase::Draw,
            TurnPhase::Draw => TurnPhase::Resource,
            TurnPhase::Resource => TurnPhase::Build,
            TurnPhase::Build => TurnPhase::Combat,
            TurnPhase::Combat => TurnPhase::Recovery,
            TurnPhase::Recovery => TurnPhase::EndTurn,
            TurnPhase::EndTurn => TurnPhase::Untap,
        }
    }
    ///This function returns a human readable name for the phase
    pub fn to_string(&self) -> String {
        match *self {
            TurnPhase::Untap => "Untap".to_string(),
            TurnPhase::Draw => "Draw".to_string(),
            TurnPhase::Resource => "Resource".to_string(),
            TurnPhase::Build => "Build".to_string(),
            TurnPhase::Combat => "Combat".to_string(),
            TurnPhase::Recovery => "Recovery".to_string(),
            TurnPhase::EndTurn => "End".to_string(),
            // _ => "blablabla".to_string(),
        }
    }
}

pub enum PlayType {
    Resource,
    Build,
    Interrupt,
    Recovery,
}

pub enum CardLocation {
    Deck,
    Hand,
    Play,
    Void,
    Hell,
}

pub enum Rarity {
    Common,
    Uncommon,
    Rare,
    Mythical,
    Epic,
    Legendary, //Unique
}
