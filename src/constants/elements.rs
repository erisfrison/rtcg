///The three element classes.

#[derive(PartialEq,Eq,Clone,Hash)]
pub enum Element {
    ///The Classical Elements
    Classical(ClassicalElement),
    ///Combined elements are the combination of two classical elements
    ///that are not polar opposites.
    Combined(CombinedElement),
    ///Any type of element that does not fit in with the classical or
    ///the combined elements. Steam is a combination between opposite
    ///classical elements and therefore belongs to this category.
    Special(SpecialElement),
}

impl Element {
    ///creates an element from a str
    pub fn from_str(e: &str) -> Element {
        // println!("creating element from str: {}",e);
        match e {
            "Air" => return Element::Classical(ClassicalElement::Air),
            "Earth" => return Element::Classical(ClassicalElement::Earth),
            "Fire" => return Element::Classical(ClassicalElement::Fire),
            "Water" => return Element::Classical(ClassicalElement::Water),
            "Day" => return Element::Special(SpecialElement::Day),
            "Night" => return Element::Special(SpecialElement::Night),
            //TODO: add the other elements.
            _ => Element::Classical(ClassicalElement::Earth)
        }
    }
    ///Returns a String describing the Element Class the element belongs to.
    pub fn get_type_string(&self) -> String {
        match self {
            &Element::Classical(_) => "Classical Element".to_string(),
            &Element::Combined(_) => "Combined Element".to_string(),
            &Element::Special(_) => "Special Element".to_string(),
        }
    }
    ///Returns a String describing the Element itself
    pub fn get_element_string(&self) -> String {
        match self {
            &Element::Classical(ref element) => element.to_string(),
            &Element::Combined(ref element) => element.to_string(),
            &Element::Special(ref element) => element.to_string(),
        }
    }
    ///Returns a String describing both the Element Class and the Element
    ///The format is "Element Class – Element"
    pub fn to_string(&self) -> String {
        self.get_type_string() + " – " + self.get_element_string().as_str()
    }
}

/// The Classical Elements
#[derive(PartialEq,Eq,Clone,Hash)]
pub enum ClassicalElement {
    ///Common for flying monsters like birds and insects
    Air,
    ///Common for most land animal, particularly mammals
    Earth,
    ///Mostly used for supernatural creatures
    Fire,
    ///Swimming things, I guess?
    Water,
}

impl ToString for ClassicalElement {
    fn to_string(&self) -> String {
        match self {
            &ClassicalElement::Air => "Air".to_string(),
            &ClassicalElement::Earth => "Earth".to_string(),
            &ClassicalElement::Fire => "Fire".to_string(),
            &ClassicalElement::Water => "Water".to_string(),
        }
    }
}

///The Combined Elements
#[derive(PartialEq,Eq,Clone,Hash)]
pub enum CombinedElement {
    ///Air+Water
    Ice,
    ///Air+Fire
    Lightning,
    ///Earth+Fire
    Magma,
    ///Earth+Water
    Poison,
}

impl ToString for CombinedElement {
    fn to_string(&self) -> String {
        match self {
            &CombinedElement::Ice => "Ice".to_string(),
            &CombinedElement::Poison => "Poison".to_string(),
            &CombinedElement::Magma => "Magma".to_string(),
            &CombinedElement::Lightning => "Lightning".to_string(),
        }
    }
}

///Additional Elements
#[derive(PartialEq,Eq,Clone,Hash)]
pub enum SpecialElement {
    ///Day
    Day,
    ///Night
    Night,
    ///Pure is somewhere between elementless and a mix between all elements.
    Pure,
    ///Fire+Water
    Steam,
}

impl ToString for SpecialElement {
    fn to_string(&self) -> String {
        match self {
            &SpecialElement::Day => "Day".to_string(),
            &SpecialElement::Night => "Night".to_string(),
            &SpecialElement::Pure => "Pure".to_string(),
            &SpecialElement::Steam => "Steam".to_string(),
        }
    }
}
