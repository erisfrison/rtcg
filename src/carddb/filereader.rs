use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::path::Path;
use symtern;
use symtern::traits::Intern;
use carddb::card::*;

use xml::reader::{EventReader, XmlEvent};
use xml::name::*;
use xml::attribute;
use constants;
use constants::Condition;
use std::cmp::PartialEq;

pub fn read_text_file(p: &str) {
    let path = Path::new(p);
    //TODO: make safe
    if !path.exists() { panic!("File {} not found!",p); } else {
        let mut f = File::open(path).unwrap();
        let mut reader = BufReader::new(f);
        let mut line = String::new();
        //TODO: make safe
        let len = reader.read_line(&mut line).unwrap();
        println!("{}",line);
    }
}

pub fn import_subtypes(p: &str,pool: &mut symtern::Pool<str,u16>) {
    let path = Path::new(p);
    //TODO: make safe
    if !path.exists() { panic!("File {} not found!",p); } else {
        let mut f = File::open(path).unwrap();
        let mut reader = BufReader::new(f);
        let mut line = String::new();
        //TODO: make safe
        let len = reader.read_line(&mut line).unwrap();
        pool.intern(line.as_str());
    }
}
//use std::str::FromStr;

//When you only care about the "type" attribute
fn get_type_attr(attributes: Vec<attribute::OwnedAttribute>) -> String {
    for attr in attributes {
        match attr.name.local_name.as_str() {
            "type" => { return attr.value.to_string(); },
            _ => { }
        }
    }
    panic!("Element does not have 'type' attribute!");
}

use std::collections::HashMap;
fn get_attributes(labels: Vec<&str>, attributes: Vec<attribute::OwnedAttribute>) -> Vec<String> {
    let mut hashmap: HashMap<String,String> = HashMap::new();
    for attr in attributes {
        let label: String = attr.name.local_name;
        if labels.contains(&label.as_str()) {
            hashmap.insert(label.to_string(),attr.value);
        }
    }
    assert!(labels.len()==hashmap.len());
    let mut values = Vec::new();
    for label in labels {
        values.push(hashmap.get(&label.to_string()).unwrap().clone());
    }
    return values;
}

#[derive(PartialEq,Debug)]
enum XmlTree {
    Card,
        Stats,
        Types,
            Subtype,
        Costs,
            Cost,
        Conditions,
            And,
            Or,
            Condition,
        Effect,
            //Conditions,
        Abilities,
            Ability,
                //Effect,
                    AbilityEffectConditions,
                AbilityConditions,
                AbilityCosts,
                    AbilityCost,
        Description,
}


///This function reads an xml file and creates a CardData structure
///with the configurations provided by the xml file.
pub fn import_card_xml(p: &str,pool:&mut symtern::Pool<str,u16>) -> CardData {
    let file = File::open(p).unwrap();
    let file = BufReader::new(file);

    let mut card_name = String::new();
    let mut set_id: usize = 0;
    let mut card_id: usize = 0;
    let mut card_type = String::new();
    let mut card_subtypes: Vec<symtern::Sym<u16>> = Vec::new();
    let mut card_attack = None;
    let mut card_health = None;
    let mut card_cost = Vec::new();
    let mut condition = Condition::True;
    let mut effects = Vec::new();
    let mut new_effect = constants::effect::Effect::Dummy;

    let parser = EventReader::new(file);
    let mut depth = 0;
    let mut branch: Vec<XmlTree> = Vec::new();
    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, attributes, .. }) => {
                match name.local_name.as_str() {
                    "card" => {
                        if !branch.is_empty() {
                            panic!("XML should start with <card>");
                        }
                        branch.push(XmlTree::Card);
                        for attr in attributes {
                            match attr.name.local_name.as_str() {
                                "name" => card_name = attr.value.to_string(),
                                "set_id" => set_id = {
                                    if let Ok(id) = attr.value.parse::<usize>() {
                                        id
                                    } else { panic!("Error, expected usize in: {}", attr); }
                                },
                                "card_id" => card_id = {
                                    if let Ok(id) = attr.value.parse::<usize>() {
                                        id
                                    }
                                    else { panic!("Error, expected usize in: {}", attr); }
                                },
                                _ => { println!("Error, unrecognized pattern in: {}",attr); }
                            }
                        }
                    },
                    "stats" => {
                        assert!(*branch.last().unwrap()==XmlTree::Card);
                        branch.push(XmlTree::Stats);
                        let values = get_attributes(vec!["health","attack"],attributes);
                        if let Ok(v) = values[0].parse::<i8>() { card_health = Some(v) };
                        if let Ok(v) = values[1].parse::<i8>() { card_attack = Some(v) };
                    },
                    "cost" => {
                        branch.push(XmlTree::Cost);
                        let values = get_attributes(vec!["type","value","amount"],attributes);
                        match values[0].as_str() {
                            "Resource" => {
                                for _ in 0..values[2].parse::<usize>().unwrap() {
                                    card_cost.push(constants::Resource::from_str(values[1].as_str()));
                                }
                            }
                            _ => {}
                        }
                    },
                    "types" => {
                        assert!(*branch.last().unwrap()==XmlTree::Card);
                        branch.push(XmlTree::Types);
                        card_type = get_type_attr(attributes);
                        // expect = "Subtype";
                    },
                    "subtype" => {
                        assert!(*branch.last().unwrap()==XmlTree::Types);
                        branch.push(XmlTree::Subtype);
                        if let Ok(symbol) = pool.intern(get_type_attr(attributes).as_str()) {
                            card_subtypes.push(symbol);
                        }
                    }
                    //depending on where we are in the xml file
                    //this could be the card playing conditions (in Card)
                    //the effect conditions (in Effect) the ability
                    //activation conditions (in Ability) or the ability
                    //effect conditions (in Ability:Effect)
                    "conditions" => {
                        branch.push(XmlTree::Conditions);
                        match branch[branch.len()-2] {
                            XmlTree::Card => {
                            },
                            XmlTree::Effect => {
                            }
                            XmlTree::Ability => {
                            }
                            _ => {
                                panic!("Conditions element is misplaced.");
                            }
                        }
                    }
                    "and" => {
                        assert!(
                            vec![XmlTree::And,
                                XmlTree::Or,
                                XmlTree::Conditions]
                            .contains(branch.last().unwrap()));
                        branch.push(XmlTree::And);
                    }
                    "or" => {
                        assert!(
                            vec![XmlTree::And,
                                XmlTree::Or,
                                XmlTree::Conditions]
                            .contains(branch.last().unwrap()));
                        branch.push(XmlTree::Or);
                    }
                    "condition" => {
                        assert!(
                            vec![XmlTree::And,
                                XmlTree::Or,
                                XmlTree::Conditions]
                            .contains(branch.last().unwrap()));
                        branch.push(XmlTree::Condition);
                        condition = Condition::Phase(constants::TurnPhase::Build);
                    }
                    "effect" => {
                        branch.push(XmlTree::Effect);
                        let values = get_attributes(vec!["type","value","target"],attributes);
                        //TODO: Currently all effects are replaced with AddToResource,
                        //include other efects
                        new_effect = constants::effect::Effect::AddToResourcePool(constants::Resource::from_str(values[1].as_str()));
                        // match branch[branch.len()-2] {
                        //     XmlTree::Card => {
                        //
                        //     }
                        //     XmlTree::Ability => {
                        //
                        //     }
                        //     _ => {
                        //         panic!("Effect element is misplaced.");
                        //     }
                        // }
                    }
                    "description" => {
                        branch.push(XmlTree::Description);

                    }
                    _ => { panic!("Error, unrecognized pattern in: {}", name ); }
                }
                depth+=1;
            }
            Ok(XmlEvent::EndElement { name }) => {
                depth-=1;
                if *branch.last().unwrap()==XmlTree::Effect && branch[branch.len()-2]==XmlTree::Card {
                    effects.push(new_effect.clone());
                }
                assert!(branch.pop().is_some());
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }
    return CardData {
        set_id: set_id,
        card_id: card_id,
        name: card_name,
        card_type: card_type,
        subtypes: card_subtypes,
        cost: card_cost,
        effects: effects,
        condition: condition,
        health: card_health,
        attack: card_attack,
    }
}

//helper function to show xml files in the terminal
fn indent(size: usize) -> String {
    const INDENT: &'static str = "    ";
    (0..size).map(|_| INDENT)
             .fold(String::with_capacity(size*INDENT.len()), |r, s| r + s)
}

pub fn print_xml(p: &str) {
    let file = File::open(p).unwrap();
    let file = BufReader::new(file);

    let parser = EventReader::new(file);
    let mut depth = 0;
    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, attributes, .. }) => {
                println!("{}+{}", indent(depth), name);
                for attr in attributes {
                    println!("{}  {}",indent(depth),attr);
                }
                depth += 1;
            }
            Ok(XmlEvent::EndElement { name }) => {
                depth -= 1;
                println!("{}-{}",indent(depth),name);
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
            _ => {}
        }
    }
}

pub fn read_databases() {
    read_text_file("./db/set_1");
    if false { print_xml("./db/earth.xml"); } //TODO: remove, i just wanted cargo to stop complaining about an unused function
}
