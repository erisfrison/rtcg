use constants::{Resource,Condition};
use constants::duration::Duration;
use constants::effect::Effect;
use symtern;
// use std::collections::BitSet;

///Stores the data of an imported card.
///This CardData is then used by the game to create the actual card when necessary.
pub struct CardData {
    pub set_id: usize,
    pub card_id: usize,
    //Name is at the top of the card.
    //Type is on the bar under the image.
    //Subtypes are placed after the type.
    pub name: String,
    pub card_type: String,
    pub subtypes: Vec<symtern::Sym<u16>>,
    pub cost: Vec<Resource>,
    //the conditions determine when the card can be played.
    pub condition: Condition,
    pub effects: Vec<Effect>,//Vec<(Effect,Duration,Condition)>,
    pub health: Option<i8>,
    pub attack: Option<i8>,
}

//TODO: Implement Serializable for this structure.
