use specs;
use specs::Join;
use abilities::duration::*;
use std::sync::RwLock;
use constants::TurnPhase;

struct DurationSystem {
    turn_phase: RwLock<TurnPhase>,
}

impl specs::System<()> for DurationSystem {
    fn run(&mut self, arg: specs::RunArg, _: ()) {
        //Go through all of the different types of duration
        //and delete their entities if the end conditions
        //have been met. TODO: this could be improved upon
        //by communicating events over a channel...
        if *self.turn_phase.read().unwrap()==TurnPhase::EndTurn {
            let (durations,entities) = arg.fetch(|world| {
                (world.read::<UntilEndOfTurn>(),
                world.entities())
            });
            for (_, e) in (&durations,&entities).join() {
                arg.delete(e)
            }
        }
    }
}
