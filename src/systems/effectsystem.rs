use specs::{Entity,World};
use constants::effect::Effect;
use constants::target::TargettingType;
use abilities::effects::*;
use abilities::targets::*;

pub fn spawn_effect(t: TargettingType, e: Effect, w: &World) -> Entity {
    let mut entity = w.create();
    match t {
        TargettingType::PlayerTarget(ep) => {
            entity = entity.with({
                PlayerTarget {
                    target: ep,
                }
            })
        }
        _ => {}
    }

    e.attach_to_entity(entity.build(),w)
}
