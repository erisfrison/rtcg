use specs;
use cards::card::Card;
use std::sync::RwLock;
use constants::TurnPhase;

struct TurnCardPlayableSystem {
    turn_phase: RwLock<TurnPhase>,
}

impl specs::System<()> for TurnCardPlayableSystem {
    fn run(&mut self, arg: specs::RunArg, _: ()) {
        let cards = arg.fetch(|world| {
            world.read::<Card>()
        });
        //This function should mark cards that are playable this turn as playable;
        //It should check whose turn it is and whether the card belongs to that player;
        //It should check whether this card can be played during the current part of turn;
        //It should check whether the player can pay for the card;
    }
}
