use specs;
use specs::Join;
use std::sync::{RwLock,Arc};
use constants::TurnPhase;
use abilities::conditions::Conditions;
use constants::Condition;

pub struct ConditionSystem {
    pub turn_phase: Arc<RwLock<TurnPhase>>,
}

///Every tick the conditionsystem checks all Conditions Components and sets
///their satisfied flag to the correct status. This flag can then be used
///to check whether a card can be played, an ability activated or whether an
///effect is currently active. (the duration of an effect is done by another
///system: the DurationSystem, which flags effects for deletion when their
///duration runs out)
impl specs::System<()> for ConditionSystem {
    fn run(&mut self, arg: specs::RunArg, _: ()) {
        let mut cs = arg.fetch(|world| {
            world.write::<Conditions>()
        });
        for c in (&mut cs).join() {
            c.satisfied = self.check_condition(&c.conditions);
        }
    }
}

impl ConditionSystem {
    fn and(&self, conditions: &Vec<Condition>) -> bool {
        let mut satisfied = true;
        for c in conditions {
            //TODO: make it check conditions
            if self.check_condition(c) {
                satisfied = true;
            }
        }
        return satisfied;
    }
    fn or(&self, conditions: &Vec<Condition>) -> bool {
        let mut satisfied = false;
        for c in conditions {
            if self.check_condition(c) {
                satisfied = true;
            }
        }
        return satisfied;
    }
    fn check_condition(&self, condition: &Condition) -> bool {
        match condition {
            &Condition::And(ref conditions) => return self.and(conditions),
            &Condition::Or(ref conditions) => return self.or(conditions),
            &Condition::Phase(ref phase) => return *self.turn_phase.read().unwrap()==*phase,
            &Condition::True => return true,
            _ => return false
        }
    }
}
