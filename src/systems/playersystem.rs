use specs::{Entity,RunArg,World,Gate,Join};
use players::player::{Player,ResourcePool};
use cards::card::Card;
use cards::cost::Cost;
use abilities::effects::addtoresourcepool::AddToResourcePool;
use abilities::targets;
use constants::Resource;
use constants;
use constants::elements::{Element,ClassicalElement};
use helper;
use systems;

///Returns all the resources the player (ep) has available for use.
pub fn get_player_available_resources(ep: Entity, world: &World) -> Vec<Resource> {
    let pools = world.read::<ResourcePool>().pass();
    let addpools = world.read::<AddToResourcePool>().pass();
    if let Some(pool_entity) = pools.get(ep) {
        let mut pool = pool_entity.resources.clone();
        let ets = targets::targets_player(ep, world);
        for (modifier,eid) in (&addpools, &world.entities()).join() {
            pool.push(modifier.resource.clone());//TODO: find better way of doing this
        }
        for r in pool_entity.in_use.clone() { //TODO: ry without clone().
            match pool.iter().position(|x| *x == r) {
                None => { panic!("Unavailable resource in use."); },
                Some(i) => { pool.remove(i); }
            }
        }
        return pool;
    }
    else {
        panic!("Expected entity to have a Player component!");
    }
}

///Attempts to make player (ep) pay resources (res), returning true if the payment
///was executed, and false if it was not.
pub fn pay_resources(ep: Entity, res: Vec<Resource>, world: &World) -> bool {
    if can_pay(ep, res.clone(), world) {
        let mut pools = world.write::<ResourcePool>().pass();
        if let Some(pool) = pools.get_mut(ep) {
            pool.in_use.append(&mut res.clone());
            return true;
        }
        else {
            panic!("Player not found.")
        }
    }
    else {
        println!("Player needs:");
        helper::print_resources(&res);
        println!("Player cannot pay resources!");
        return false;
    }
}

///Checks if the player (ep) has resources (res).
///ep: Player Entity
pub fn can_pay(ep: Entity, res: Vec<Resource>, world: &World) -> bool {
    let mut resources = get_player_available_resources(ep,world);
    assert!(Resource::SoulToken == Resource::SoulToken,"test");
    for r in res {
        match resources.iter().position(|x| *x == r) {
            None => { return false },
            Some(i) => { resources.remove(i); },
        }
    }
    return true;
}

///Reset available resources for player (ep).
///Also sets essence_played to false.
///TODO: maybe this should be a separate function?
///And then both should be called during the untap phase?
///ep: Player Entity
pub fn untap_resources(ep: Entity, world: &World) {
    let mut pools = world.write::<ResourcePool>().pass();
    if let Some(p) = pools.get_mut(ep) {
        p.in_use.clear();
    }
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        p.played_essence = false;
    }
}

pub fn add_resource(ep: Entity, res: Resource, world: &World) {
    let mut pools = world.write::<ResourcePool>().pass();
    if let Some(p) = pools.get_mut(ep) {
        p.resources.push(res);
    }
    else {
        panic!("Player either does not exist, or does not have a resource pool!");
    }
}

///Make player (ep) draw a card. If the player has no more cards in the deck,
///the out_of_cards flag will be set to true.
///ep: Entity should be a player entity
pub fn draw_card(ep: Entity, world: &World) -> bool {
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        if let Some(card) = p.deck.pop() {
            //Change owner of the card to player
            let mut cards = world.write::<Card>().pass();
            if let Some(c) = cards.get_mut(card) {
                c.owner = Some(ep);
            }

            //Put card in player's hand
            p.hand.push(card);
            return true;
        }
        else {
            p.out_of_cards=true;
            return false;
        }
    }
    else {
        panic!("Expected entity to have a Player component.");
    }
}

pub fn play_card(ep: Entity, ec: Entity, world: &mut World) {
    //Make it use the cost of the card, if there is one attached to it.
    let paid = {
        let costs = world.read::<Cost>().pass();
        let cost = {
            if let Some(c) = costs.get(ec) {
                c.resources.clone()
            }
            else {
                Vec::new()
            }
        };
        //Verify the player has paid to play the card.
        pay_resources(ep,cost,world)
    };
    if paid {
        let mut players = world.write::<Player>().pass();
        if let Some(p) = players.get_mut(ep) {
            //Check if player can play card (ie. has the card in hand and the rules allow it)
            if let Some(index) = p.hand.iter().position(|x| *x == ec) {
                p.hand.remove(index);

                //resolve card effects.
                let cards = world.read::<Card>().pass();
                if let Some(c) = cards.get(ec) {
                    for i in 0..c.effects.len() {
                        systems::effectsystem::spawn_effect(constants::target::TargettingType::PlayerTarget(ep),c.effects[i].clone(),world);
                    }
                }

                //TODO: currently the card is immediately moved to the hell
                //this should only be the case for spells and interrupts.
                //and even these should only be moved there after stack resolve.
                p.hell.push(ec);
                println!("Played card!");
            }
        }
    }
}

///ep: Player Entity; ec: Card Entity
pub fn add_to_deck(ep: Entity, ec: Entity, world: &World) {
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        p.deck.push(ec);
    }
}

///ep: Player Entity; ec: Card Entity
pub fn discard_card(ep: Entity, ec: Entity, world: &World) {
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        // TODO: use this code instead when remove_item is merged with Rust stable.
        // if let Some(c) = p.hand.remove_item(&ec) {
        //     p.hell.push(c);
        // }
        // Temporary replacement code:
        if p.hand.contains(&ec) {
            p.hand.retain(|&x| x!=ec);
            p.hell.push(ec);
        }

    }
}

///ep: Player Entity
pub fn discard_random_card(ep: Entity, world: &World) {
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        //TODO: use random
        if p.hand.len()>0 {
            let ec = p.hand.remove(0);
            p.hell.push(ec);
        }
        else {
            panic!("Expected player (ep: {}) to have at least one card.");
        }
    }
}

pub fn shuffle_deck(ep: Entity, world: &World) {
    let mut players = world.write::<Player>().pass();
    if let Some(p) = players.get_mut(ep) {
        p.shuffle_deck();
    } else { panic!("Player not found!"); }
}

///ep: Player Entity
pub fn get_player_hand(ep: Entity, world: &World<()>) -> Vec<Entity> {
    let players = world.read::<Player>().pass();
    // for p in (&players).join() {
    //     println!("{}",p.name);
    // }
    if let Some(p) = players.get(ep) {
        return p.hand.clone();
    }
    else {
        panic!("Player (ep: {}) does not exist!",ep.get_id());
    }
}

pub fn get_player_deck_size(ep: Entity, w: &World<()>) -> usize {
    let players = w.read::<Player>().pass();
    if let Some(p) = players.get(ep) {
        return p.deck.len();
    }
    else {
        panic!("Player (ep: {}) does not exist!", ep.get_id());
    }
}
